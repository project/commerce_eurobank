<?php

/**
 * @file
 * Provides a Eurobank API (via XML) payment method for Drupal Commerce (not redirection).
 */

// Eurobank transaction mode definitions.
define('EUROBANK_TXN_MODE_LIVE', 'live');
define('EUROBANK_TXN_MODE_TEST', 'test');

// Eurobank Server URLs for Simulation, Test and Live environments.
define('EUROBANK_SERVER_LIVE', 'https://ep.eurocommerce.gr/proxypay/apacsonline');
define('EUROBANK_SERVER_TEST', 'https://eptest.eurocommerce.gr/proxypay/apacsonline');

/**
 * Implements hook_commerce_payment_method_info().
 */
function commerce_eurobank_commerce_payment_method_info() {
  $payment_methods = array();

  $payment_methods['commerce_eurobank'] = array(
    'title' => t('Eurobank API'),
    'short_title' => t('Eurobank API'),
    'display_title' => t('Credit Card (VISA / MasterCard)'),
    'description' => t('Integration with Eurobank API.'),
    'active' => TRUE,
    'callbacks' => array(),
  );

  return $payment_methods;
}

/**
 * Settings form for Eurobank API payment method.
 *
 * Used to set merchant id and hash password within Rules settings.
 */
function commerce_eurobank_settings_form($settings = NULL) {
  $form = array();

  // Merge default settings into the stored settings array.
  $settings = (array) $settings + array(
    'merchant_id' => '',
    'hash_password' => '',
    'txn_mode' => EUROBANK_TXN_MODE_TEST,
    'txn_type' => COMMERCE_CREDIT_AUTH_CAPTURE,
  );

  $form['merchant_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Merchant ID'),
    '#description' => t('This is the merchant ID that Eurobank sent you when you set up your account.'),
    '#default_value' => $settings['merchant_id'],
    '#required' => TRUE,
  );

  $form['hash_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Hash password'),
    '#description' => t('This is the hash password that Eurobank sent you.'),
    '#default_value' => $settings['hash_password'],
    '#required' => TRUE,
  );

  $form['txn_mode'] = array(
    '#type' => 'radios',
    '#title' => t('Transaction mode'),
    '#description' => t('Adjust to live transactions when you are ready to start processing actual payments.'),
    '#options' => array(
      EUROBANK_TXN_MODE_LIVE => t('Live transactions in a live account'),
      EUROBANK_TXN_MODE_TEST => t('Test transactions in a test account'),
    ),
    '#default_value' => $settings['txn_mode'],
  );

  $form['txn_type'] = array(
    '#type' => 'radios',
    '#title' => t('Default credit card transaction type'),
    '#description' => t('The default will be used to process transactions during checkout.'),
    '#options' => array(
      COMMERCE_CREDIT_AUTH_CAPTURE => t('Authorisation and capture'),
      COMMERCE_CREDIT_AUTH_ONLY => t('Authorisation only (requires manual or automated capture after checkout)'),
    ),
    '#default_value' => $settings['txn_type'],
  );

  return $form;
}

/**
 * Payment method callback: checkout form.
 */
function commerce_eurobank_submit_form($payment_method, $pane_values, $checkout_pane, $order) {
  module_load_include('inc', 'commerce_payment', 'includes/commerce_payment.credit_card');

  $credit_card_settings = array(
    'type' => array(
      'visa',
      'mastercard',
    ),
    'number' => '',
    'code' => '',
  );
  return commerce_payment_credit_card_form($credit_card_settings);
}

/**
 * Payment method callback: checkout form validation.
 */
function commerce_eurobank_submit_form_validate($payment_method, $pane_form, $pane_values, $order, $form_parents = array()) {
  module_load_include('inc', 'commerce_payment', 'includes/commerce_payment.credit_card');

  // Validate the credit card fields.
  $settings = array(
    'form_parents' => array_merge($form_parents, array('credit_card')),
  );

  if (!commerce_payment_credit_card_validate($pane_values['credit_card'], $settings)) {
    return FALSE;
  }
}

/**
 * Payment method callback: checkout form submission.
 */
function commerce_eurobank_submit_form_submit($payment_method, $pane_form, $pane_values, &$order, $charge) {
  // Create request to send to Eurobank.
  $transaction = array();

  $transaction_type = ($payment_method['settings']['txn_type'] == COMMERCE_CREDIT_AUTH_ONLY) ? 'AUTH' : 'CAPTURE';

  if (!isset($payment_method['settings']['merchant_id']) && !isset($payment_method['settings']['hash_password'])) {
    watchdog('commerce_eurobank',  '%method : Merchant id and hash password have not been set in the payment settings. Cannot proceed with transaction', array('%method' => $payment_method['settings']['txn_mode']), WATCHDOG_ERROR);
    drupal_set_message(t('The Eurobank gateway has not been correctly configured - see watchdog for details. Cannot proceed with payment'), 'error');
    return FALSE;
  }

  $customer_email = $order->mail;
  $merchant_ref = $order->order_id . '_' . $customer_email . '_' . date('U');
  $my_amount = $charge['amount'];

  if ($charge['currency_code'] == 'EUR') {
    $currency = 978;
  }
  elseif ($charge['currency_code'] == 'USD') {
    $currency = 840;
  }
  elseif ($charge['currency_code'] == 'GBP') {
    $currency = 826;
  }

  $cc_number = $pane_values['credit_card']['number'];
  $cc_exp = $pane_values['credit_card']['exp_month'] . drupal_substr($pane_values['credit_card']['exp_year'], 2, 2);
  $cccv = $pane_values['credit_card']['code'];
  $card_type = ($pane_values['credit_card']['type'] == 'mastercard') ? 'MC' : 'VISA';
  $merchant_id = $payment_method['settings']['merchant_id'];
  $hash_password = $payment_method['settings']['hash_password'];
  $merchant_desc = 'site payment';

  $post = "APACScommand=NewRequest&Data=<?xml version=\"1.0\" encoding=\"UTF-8\"?><JProxyPayLink><Message><Type>PreAuth</Type><Authentication><MerchantID>$merchant_id</MerchantID><Password>$hash_password</Password></Authentication><OrderInfo><Amount>$my_amount</Amount><MerchantRef>$merchant_ref</MerchantRef><MerchantDesc>$merchant_desc</MerchantDesc><Currency>$currency</Currency><CustomerEmail>$customer_email</CustomerEmail><Var1>var1</Var1><Var2>var2</Var2><Var3>var3</Var3><Var4>var4</Var4><Var5>var5</Var5><Var6>var6</Var6><Var7>var7</Var7><Var8>var8</Var8><Var9>var9</Var9></OrderInfo><PaymentInfo><CCN>$cc_number</CCN><Expdate>$cc_exp</Expdate><CVCCVV>$cccv</CVCCVV><InstallmentOffset>0</InstallmentOffset><InstallmentPeriod>0</InstallmentPeriod></PaymentInfo></Message></JProxyPayLink>";

  // Determine the correct url based on the transaction mode.
  $server_url = ($payment_method['settings']['txn_mode'] == EUROBANK_TXN_MODE_LIVE) ? EUROBANK_SERVER_LIVE : EUROBANK_SERVER_TEST;

  // Send auth post.
  set_time_limit(60);
  $curl_session = curl_init();
  curl_setopt($curl_session, CURLOPT_URL, $server_url);
  curl_setopt($curl_session, CURLOPT_HEADER, 0);
  curl_setopt($curl_session, CURLOPT_POST, 1);
  curl_setopt($curl_session, CURLOPT_POSTFIELDS, $post);
  curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl_session, CURLOPT_TIMEOUT, 30);
  curl_setopt($curl_session, CURLOPT_SSL_VERIFYPEER, FALSE);
  curl_setopt($curl_session, CURLOPT_SSL_VERIFYHOST, 1);

  $raw_response = curl_exec($curl_session);

  // Check auth response.
  preg_match('%<ERRORCODE>.*</ERRORCODE>%i', $raw_response, $error_code);
  preg_match('%<ERRORMESSAGE>.*</ERRORMESSAGE>%i', $raw_response, $error_message);
  $error_num = drupal_substr($error_code[0], 11, -12);
  $error_msg = drupal_substr($error_message[0], 14, -15);

  // Remove cc number from log message.
  $safe_post = preg_replace('%<CCN>.*</CCN>%i', '<CCN>CC NUMBER NOT SHOWN</CCN>', $post);

  // Remove ccv from log message.
  $safe_post = preg_replace('%<CVCCVV>.*</CVCCVV>%i', '<CVCVV>XXX</CVCCVV>', $safe_post);

  // Remove expiration date from log message.
  $safe_post = preg_replace('%<Expdate>.*</Expdate>%i', '<Expdate>XXXX</Expdate>', $safe_post);

  watchdog('commerce_eurobank', "%payment_method auth data: server url: %server_url<br />postdata: %safe_post<br />response: %raw_response<br />error number: %error_num<br />
error message: %error_msg", array('%payment_method' => $payment_method['settings']['txn_mode'], '%server_url' => $server_url, '%safe_post' => htmlentities($safe_post), '%raw_response' => htmlentities($raw_response), '%error_num' => $error_num, '%error_msg' => $error_msg), WATCHDOG_INFO);

  $transaction_successful = FALSE;

  if ($error_num == '0') {
    // We had a successful auth.
    switch ($transaction_type) {
      case 'AUTH':
        // If transaction type in options is auth, then no other command needs to be sent to the bank.
        commerce_eurobank_transaction($order, $error_msg, $error_num);
        $transaction_successful = TRUE;
        break;

      case 'CAPTURE':
        // If transaction type in options is capture, then we need to send the capture command to the bank after a successful auth.
        $capture_post = "APACScommand=NewRequest&Data=<?xml version=\"1.0\" encoding=\"UTF-8\"?><JProxyPayLink><Message><Type>Capture</Type><Authentication><MerchantID>$merchant_id</MerchantID><Password>$hash_password</Password></Authentication><OrderInfo><Amount>$my_amount</Amount><MerchantRef>$merchant_ref</MerchantRef><MerchantDesc /><Currency /><CustomerEmail /><Var1 /><Var2 /><Var3 /><Var4 /><Var5 /><Var6 /><Var7 /><Var8 /><Var9 /></OrderInfo></Message></JProxyPayLink>";
        // Send Capture Command.
        curl_setopt($curl_session, CURLOPT_POSTFIELDS, $capture_post);
        $capture_raw_response = curl_exec($curl_session);
        watchdog('commerce_eurobank', "%payment_method capture post data: %capture_post<br />result: %capture_raw_response", array('%payment_method' => $payment_method['settings']['txn_mode'], '%capture_post' => htmlentities($capture_post), '%capture_raw_response' => htmlentities($capture_raw_response)), WATCHDOG_INFO);
        preg_match('%<ERRORCODE>.*</ERRORCODE>%i', $capture_raw_response, $capture_error_code);
        preg_match('%<ERRORMESSAGE>.*</ERRORMESSAGE>%i', $capture_raw_response, $capture_error_message);
        $capture_error_num = drupal_substr($error_code[0], 11, -12);
        $capture_error_msg = drupal_substr($error_message[0], 14, -15);

        if ($capture_error_num == '0') {
          // We had a successful capture.
          commerce_eurobank_transaction($order, $capture_error_msg, $capture_error_num);
          $transaction_successful = TRUE;
        }
        else {
          watchdog('commerce_eurobank', '%payment_method Successful Auth but Failed Capture!!! CHECK IT. YOU MAY HAVE TO CAPTURE MANUALLY!<br />response: %capture_raw_response', array('%payment_method' => $payment_method['settings']['txn_mode'], '%capture_raw_response' => htmlentities($capture_raw_response)), WATCHDOG_CRITICAL);

          // Send mail to site owner to inform about failed capture.
          $module = 'commerce_eurobank';
          $key = $order->order_id;
          $language = language_default();
          $params = array();
          $from = NULL;
          $email = variable_get('site_mail');
          $send = FALSE;
          $message = drupal_mail($module, $key, $email, $language, $params, $from, $send);
          $message['subject'] = t('successful auth but failed capture');
          $message['body'] = t('We had a successful auth result, but capture failed! You may have to capture money manually! Check Eurobank Back Office!');

          // Retrieve the responsible implementation for this message.
          $system = drupal_mail_system($module, $key);

          // Format the message body.
          $message = $system->format($message);

          // Send e-mail.
          $system->mail($message);
        }
        break;
    }
  }

  curl_close($curl_session);

  // If the payment failed, display an error and rebuild the form.
  if (!$transaction_successful) {
    drupal_set_message(t('Please enter you information again or try a different card. %message', array('%message' => $error_msg)), 'error');
    watchdog('commerce_eurobank', '%payment_method Payment failed: %message', array('%payment_method' => $payment_method['settings']['txn_mode'], '%message' => $error_msg), WATCHDOG_WARNING);
    drupal_set_message(t('Payment failed: %message', array('%message' => $error_msg)), 'error');
    drupal_goto('checkout');
  }
  else {
    return TRUE;
  }
}

/**
 * Create a transaction and associate it with an order.
 */
function commerce_eurobank_transaction($order, $error_msg, $error_num) {
  $transaction = commerce_payment_transaction_new('commerce_eurobank', $order->order_id);
  $transaction->amount = $order->commerce_order_total['und'][0]['amount'];
  $transaction->currency_code = $order->commerce_order_total['und'][0]['currency_code'];

  // Set a status for the payment - one of COMMERCE_PAYMENT_STATUS_SUCCESS, COMMERCE_PAYMENT_STATUS_PENDING or COMMERCE_PAYMENT_STATUS_FAILURE.
  switch ($error_num) {
    case '0':
      $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
      break;

    default:
      $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
  }
  $transaction->message = $error_msg;
  commerce_payment_transaction_save($transaction);
}
